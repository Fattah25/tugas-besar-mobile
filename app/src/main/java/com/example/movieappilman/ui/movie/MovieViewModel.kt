package com.example.movieappilman.ui.movie

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.example.movieappilman.core.domain.usecase.movie.MovieUseCase

//import com.project.proyek_akhir_mobile_programming.core.domain.usecase.movie.MovieUseCase

class MovieViewModel(private val useCase: MovieUseCase): ViewModel() {
    fun getMovies() = useCase.getMovies().asLiveData()
}