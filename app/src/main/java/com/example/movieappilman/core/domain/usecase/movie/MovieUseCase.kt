package com.example.movieappilman.core.domain.usecase.movie

import com.example.movieappilman.core.data.remote.Resource
import com.example.movieappilman.core.domain.model.Movie
import com.example.movieappilman.core.domain.model.TvShow
import kotlinx.coroutines.flow.Flow

interface MovieUseCase{
    fun getMovies(): Flow<Resource<List<Movie>>>
    fun getTvShow(): Flow<Resource<List<TvShow>>>
}